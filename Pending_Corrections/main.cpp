#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_syswm.h> 
#include <GL/gl.h>
#include <GL/glu.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <sys/types.h>
#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <fstream>
#include <ctime>
#include <cstring>
#include<dirent.h>
#include <cmath>
#include "box.gouda"
#include "cam.gouda"
#include "sdlglutils.gouda"

#define FPS 30

using namespace std;

void DrawGL(Box * b);
void quit();
double angleZ = 0;
double angleX = 0;
bool poke=false;
string menu(string spath);
Cam* camera=NULL;
SDL_Rect position;
SDL_Surface * screen=NULL;
float golden=(1+sqrt(5))/2.;
int height=480;
int width=640;
string path;

int main(int argc, char *argv[])
{
	string temp(argv[0]);
	path=temp.substr(0,temp.length()-7);
	bool pause=false;
	if(argc>1)
	{
	if(strcmp(argv[1],"--poke")==0) 
		poke=true;			
	else
      poke=false;
	}
	srand(time(NULL));

    SDL_Event event;
	const Uint32 time_per_frame = 1000/FPS;
    SDL_Init(SDL_INIT_VIDEO);
    atexit(quit);
	temp=path+"feather.png";
	const char * cpath=temp.c_str();
    SDL_WM_SetCaption("CatBird", NULL);
	SDL_WM_SetIcon(IMG_Load(cpath), NULL);


	TTF_Init();
	//string data=menu(path);
	//cout<<endl<<data<<endl;
//cout<<"test1"<<endl;
//SDL_FreeSurface(screen);
//screen = SDL_SetVideoMode(width, height, 32, SDL_OPENGL | SDL_RESIZABLE );
	/*SDL_SysWMinfo info;
	cout<<info.info.x11.window<<endl;
	SDL_VERSION(&info.version);
	if (SDL_GetWMInfo(&info))
		cout<<endl<<"ZBRAAA!"<<endl;
	cout<<&info.info.x11.window<<endl;
	info.info.x11.lock_func();
	Display *dis=info.info.x11.display;
	Window winn=info.info.x11.window;
	XSizeHints *hints  = XAllocSizeHints();
	hints->flags = PMinSize;
	hints->min_width  = 640;
	hints->min_height = 480;
	XSetWMNormalHints(dis, winn, hints);
	XSetWMSizeHints(dis, winn, hints, PMinSize);
	info.info.x11.unlock_func();
	if(XGetNormalHints(dis,winn,hints))
		cout<<endl<<hints->min_height<<endl;
	
	XFree(hints);*/

   
    Uint32 last_time = SDL_GetTicks();
    Uint32 current_time,elapsed_time;
    Uint32 start_time, stop_time;
	height=480;
	width=640;
for(;;){
	
	SDL_FreeSurface(screen);

//gluPerspective(70,(double)width/height,0.001,1000);
	screen = SDL_SetVideoMode(width, height, 32, SDL_OPENGL );
	string data=menu(path);//cout<<endl<<data<<endl;
	SDL_FreeSurface(screen);
	screen = SDL_SetVideoMode(width, height, 32, SDL_OPENGL | SDL_RESIZABLE| SDL_ASYNCBLIT );
	glMatrixMode( GL_MODELVIEW );
	glLoadIdentity();
   			glMatrixMode( GL_PROJECTION );
			glLoadIdentity();
	gluPerspective(70,(double)width/height,0.001,1000);
    glEnable(GL_DEPTH_TEST);
	Box b(path,poke,data,width,height);
	   b.DrawBox();

	bool key[8]={0,0,0,0,0,0,0,0};
	int cmode=0;
	double maxa=1e-15;
	double maxv=300;

		int x=b.getx();
	int y=b.gety();
	int z=b.getz();
	camera = new Cam(sqrt((x*x+y*y+z*z)/golden));
    camera->setScrollSensivity(0.4);
SDL_EnableUNICODE(1);
int first=0;
bool bexit=false;
    do
    {
	if(first<2)		//this trick avoid a bug when moving the menu window
	{				//laziness avoid me to find the real problem
    SDL_Event sdlevent;
    sdlevent.type = SDL_VIDEORESIZE;
    sdlevent.resize.h = height;	
	sdlevent.resize.w = width;
    SDL_PushEvent(&sdlevent);
	first++;
	}
		bexit=false;
        start_time = SDL_GetTicks();
        while (SDL_PollEvent(&event))
        {

            switch(event.type)
            {
                case SDL_QUIT:
				
                exit(0);
                break;
                case SDL_MOUSEMOTION:
		
                camera->mousemotion(event.motion);
                break;
                case SDL_MOUSEBUTTONUP:
                case SDL_MOUSEBUTTONDOWN:
				camera->mouseclick(event.button);
				if(event.button.button == SDL_BUTTON_RIGHT and event.type == SDL_MOUSEBUTTONDOWN)
				{
					cmode++;
					if(cmode==3)
						cmode=0;
					b.setcolormode(cmode);
				}
                break;
               case SDL_KEYDOWN:
				b.selectgraphon(event.key);
                switch (event.key.keysym.sym)

                {
                    case SDLK_SPACE:
						b.pauseevent();
                    break;
                    case SDLK_ESCAPE:
                  //  exit(0);
					bexit=true;
                    break;
					case SDLK_UP :
						key[0]=true;
					break;
					case SDLK_DOWN :
						key[1]=true;
					break;
					case SDLK_RIGHT :
						key[2]=true;
					break;
					case SDLK_LEFT :
						key[3]=true;
					break;
					case SDLK_KP_PLUS :
						key[4]=true;
					break;
					case SDLK_KP_MINUS :
						key[5]=true;
					break;
					case SDLK_a :
						b.setcolormode(2);
						cmode=2;
					break;
					case SDLK_v :
						b.setcolormode(1);
						cmode=1;
					break;
					case SDLK_n :
						b.setcolormode(0);
						cmode=0;
					break;
					case SDLK_PAGEDOWN :
						key[6]=true;
					break;
					case SDLK_PAGEUP :
						key[7]=true;
					break;
					case SDLK_r :
						b.SwitchRMode();
						break;
					case SDLK_g :
						b.switchgraphon();
						break;
                }

                break;
				case SDL_KEYUP:
				switch (event.key.keysym.sym)

                {
					case SDLK_UP :
						key[0]=false;
					break;
					case SDLK_DOWN :
						key[1]=false;
					break;
					case SDLK_RIGHT :
						key[2]=false;
					break;
					case SDLK_LEFT :
						key[3]=false;
					break;
					case SDLK_KP_PLUS :
						key[4]=false;
					break;
					case SDLK_KP_MINUS :
						key[5]=false;
					break;
					case SDLK_PAGEDOWN :
						key[6]=false;
					break;
					case SDLK_PAGEUP :
						key[7]=false;
					break;
                }
				break;
				case SDL_VIDEORESIZE:

                width = event.resize.w; // <- largeur
                height = event.resize.h; // <- hauteur	
/*int tempx,tempy;
					SDL_GetMouseState(&tempx,&tempy);	*/		
				if(height<480)
					{
						height=480;
						
		
					}
				 if(width<640)
				{
					width=640;
				}
				{
			/*info.info.x11.lock_func();
			hints->flags = PMinSize;
			hints->min_width = 640;
			hints->min_height = 480;
			XSetWMNormalHints(dis, winn, hints);
			XSetWMSizeHints(dis, winn, hints, PMinSize);
			info.info.x11.unlock_func();
			if(XGetNormalHints(dis,winn,hints))
			cout<<endl<<hints->min_height<<endl;
			XFree(hints);*/
			SDL_FreeSurface(screen);
           	screen = SDL_SetVideoMode(width ,height,32,SDL_OPENGL | SDL_RESIZABLE | SDL_ASYNCBLIT);
			/*info.info.x11.lock_func();*/
			//XSizeHints *hints  = XAllocSizeHints();
			/*hints->flags = PMinSize;
			hints->min_width = width;
			hints->min_height = height;
			XSetWMNormalHints(dis, winn, hints);
			XSetWMSizeHints(dis, winn, hints, PMinSize);*/
		/*	if(XGetNormalHints(dis,winn,hints))
				cout<<endl<<hints->min_height<<endl;
			XFree(hints);
			info.info.x11.unlock_func();*/
			b.Setwh(width,height);
			glViewport(0,0,width,height);
   			glMatrixMode( GL_PROJECTION );
			glLoadIdentity();
			gluPerspective(70,(double)width/height,0.001,1000);
			}
			break;
            }
        }
		if(key[0]==true)
			camera->uparrow();
		if(key[1]==true)
			camera->downarrow();
		if(key[2]==true)
			camera->leftarrow();
		if(key[3]==true)
			camera->rightarrow();
		if(key[4]==true)
			camera->plus();
		if(key[5]==true)
			camera->minus();

			if(key[6]==true)
			{
				if(cmode==2)
				{
				maxa-=1e-13;
				if(maxa<1e-13)
					maxa=1e-13;
				b.setmaxa(maxa);
				}
				if(cmode==1)
				{
				maxv-=10;
				if(maxv<1)
					maxv=1;
				b.setmaxv(maxv);
				}
			}
			if(key[7]==true)
			{
				if(cmode==2)
				{
				maxa+=5e-14;
				b.setmaxa(maxa);
				}
				if(cmode==1)
				{
				maxv+=10;
				b.setmaxv(maxv);
				}
			}		
		
        current_time = SDL_GetTicks();
        elapsed_time = current_time - last_time;
        last_time = current_time;
		if(pause==false)
		{
			//newcoord
		}
		DrawGL(&b);
        elapsed_time = SDL_GetTicks() - start_time;
        stop_time = SDL_GetTicks();
		//cout<<1/((stop_time - last_time)/1000.)<<endl;
		//if(1/((stop_time - last_time)/1000.)<30)
			//cout<<"pfff		"<<1/((stop_time - last_time)/1000.)<<endl;
		if((stop_time - last_time) < time_per_frame)
        {
            SDL_Delay(time_per_frame - (stop_time - last_time));
        }


    }while(bexit==false);
	delete camera;
	}
    return 0;
}

void DrawGL(Box * b)
{
	int color[3]={255,0,0};
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    glMatrixMode( GL_MODELVIEW );
    glLoadIdentity( );
	camera->gogogo();
	glTranslated(-b->getx()/2.,-b->gety()/2.,-b->getz()/2.);
	b->DrawBox();
	//cout<<endl<<"1"<<endl;
	b->DrawClooneys();
	//cout<<endl<<"2"<<endl;
	glTranslated(b->getx()/2.,b->gety()/2.,b->getz()/2.);
	//cout<<endl<<"3"<<endl;
	b->HUD();
	//cout<<endl<<"4"<<endl;
	b->NextStep();
	//cout<<endl<<"5"<<endl;
   SDL_GL_SwapBuffers();

}


string menu(string path)
{
   glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

glEnable(GL_COLOR_MATERIAL);
	string temppath=path+"../Data/";
	string stemp;
	string lineone;
	string linetwo;
	struct dirent *pDirent;
	DIR *pDir;
	pDir=opendir(temppath.c_str());
	int count=0;
        while ((pDirent = readdir(pDir)) != NULL) {
           if ((strchr(pDirent->d_name, '.')) == NULL)
				count++;
        }
		
   		 closedir(pDir);
		pDir=opendir(temppath.c_str());
		struct dirent *pDirenta[count];
		int j=0;
		while ((pDirent = readdir(pDir)) != NULL) {
           if ((strchr(pDirent->d_name, '.')) == NULL)
				{pDirenta[j]=pDirent;j++;}
        }
	string temp=path+"../Data/"+pDirenta[0]->d_name+"/Init.txt";
	ifstream fileselect(temp.c_str());
fileselect>>stemp;
lineone=stemp;
fileselect>>setw(6)>>stemp;
lineone+=" : "+stemp+"		";
fileselect>>setw(6)>>stemp;
lineone+="dt : "+stemp+"ps";
//cout<<endl<<lineone<<endl;	
fileselect>>setw(6)>>stemp;
linetwo="It : "+stemp+"		";
fileselect>>setw(6)>>stemp;
linetwo+="T : "+stemp+"K";
//cout<<endl<<linetwo<<endl;	
fileselect.close();
//sstemp<<"N=";fileselect>>sstemp;sstemp<<"		"<<"dt=";fileselect>>sstemp;sstemp<<"s";
//	lineone=sstemp.str();
	
	
	//for(int i=0;i<count;i++)
		//cout<<endl<<"Init : "<<pDirenta[i]->d_name<<endl;
	//screenM = SDL_SetVideoMode(640 ,480,32,SDL_OPENGL |SDL_ASYNCBLIT);
    bool go = true;
    SDL_Event event;
	int begin=0;
	int end=0;
	if(count>=6){
		end=6;}
	else
		end=count;

	TTF_Font*	fontsel;
	TTF_Font* fontunsel;
	TTF_Font* fontinfo;
	temp=path+"GeosansLight.ttf";
	const char * cpath=temp.c_str();
	fontsel = TTF_OpenFont(cpath, 50);
	fontunsel = TTF_OpenFont(cpath, 30);
	fontinfo=TTF_OpenFont(cpath,20);
	int sel=0;
	SDL_Surface * sFontmen=NULL;	//need to close this
	SDL_Color color;
unsigned long int uptimeup=0;
unsigned long int uptimedown=0;
    while (go)
    {

		//here we can do what we want
   // glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	glDepthMask(GL_FALSE);  // disable writes to Z-Buffer
	glDisable(GL_DEPTH_TEST);  // disable depth-testing*/
	glTranslated(-320,-240,0);
//background
		glBegin(GL_QUADS);
		glColor3ub(200,200,255);
		glVertex2f(0,0);
		glVertex2f( 0,480);
		glVertex2f(640,480 );
		glVertex2f(640,0);
		glEnd();
//text
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, width, 0, height); 
    glEnable(GL_TEXTURE_2D);
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	glDepthMask(GL_FALSE);  // disable writes to Z-Buffer
	glDisable(GL_DEPTH_TEST);  // disable depth-testing

	glRotatef(180, 1, 0, 0.0f); // Rotate our object around the x axis  
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	GLuint texture;
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
j=begin;

/////////////////////////Render dir name
		int tempy=400;
		int yinfo=0;
		int xinfo=0;
		do{
		if(fontsel && fontunsel){
		if(j==sel)	//big font
		{
			tempy-=10;
			color={50,50,255,0};
			sFontmen=TTF_RenderText_Blended(fontsel, pDirenta[j]->d_name, color);
			tempy-=40;
			yinfo=sFontmen->h;
			xinfo=sFontmen->w;
		}
		else	//normal font
		{
			color={0,0,0,0};
			sFontmen=TTF_RenderText_Blended(fontunsel, pDirenta[j]->d_name, color);
			tempy-=30;
		}
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, sFontmen->w, sFontmen->h, 0, GL_BGRA, 	GL_UNSIGNED_BYTE, sFontmen->pixels);
	glBegin(GL_QUADS);
	{
		glTexCoord2f(0,0); glVertex2f(100, tempy);
		glTexCoord2f(1,0); glVertex2f(100 + sFontmen->w, tempy);
		glTexCoord2f(1,1); glVertex2f(100 + sFontmen->w, tempy + sFontmen->h);
		glTexCoord2f(0,1); glVertex2f(100, tempy + sFontmen->h);
	}
	glEnd();
	//TTF_CloseFont(font); put this in the destructor
	SDL_FreeSurface(sFontmen);
	if(j==sel)
	{
	color={0,0,0};
	sFontmen=TTF_RenderText_Blended(fontinfo, linetwo.c_str(), color);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, sFontmen->w, sFontmen->h, 0, GL_BGRA, 	GL_UNSIGNED_BYTE, sFontmen->pixels);
	glBegin(GL_QUADS);
	{
		glTexCoord2f(0,0); glVertex2f(150+xinfo, tempy+yinfo/2.);
		glTexCoord2f(1,0); glVertex2f(150+xinfo + sFontmen->w,tempy+ yinfo/2.);
		glTexCoord2f(1,1); glVertex2f(150+xinfo + sFontmen->w,tempy+ yinfo/2. + sFontmen->h);
		glTexCoord2f(0,1); glVertex2f(150+xinfo, tempy+ yinfo/2. + sFontmen->h);
	}
	glEnd();
	//TTF_CloseFont(font); put this in the destructor
	SDL_FreeSurface(sFontmen);
	yinfo-=sFontmen->h+10;
	sFontmen=TTF_RenderText_Blended(fontinfo, lineone.c_str(), color);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, sFontmen->w, sFontmen->h, 0, GL_BGRA, 	GL_UNSIGNED_BYTE, sFontmen->pixels);
	glBegin(GL_QUADS);
	{
		glTexCoord2f(0,0); glVertex2f(150+xinfo, tempy+yinfo/2.);
		glTexCoord2f(1,0); glVertex2f(150+xinfo + sFontmen->w,tempy+ yinfo/2.);
		glTexCoord2f(1,1); glVertex2f(150+xinfo + sFontmen->w,tempy+ yinfo/2. + sFontmen->h);
		glTexCoord2f(0,1); glVertex2f(150+xinfo, tempy+ yinfo/2. + sFontmen->h);
	}
	glEnd();
	//TTF_CloseFont(font); put this in the destructor
	SDL_FreeSurface(sFontmen);
	}
	}


		j++;
		tempy-=20;
		}while(j<end and j<count);
	//render Titler
			color={0,0,0,0};
			sFontmen=TTF_RenderText_Blended(fontunsel, "Select a Directory", color);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, sFontmen->w, sFontmen->h, 0, GL_BGRA, 	GL_UNSIGNED_BYTE, sFontmen->pixels);
	glBegin(GL_QUADS);
	{
		glTexCoord2f(0,0); glVertex2f(320-0.5*sFontmen->w, 5);
		glTexCoord2f(1,0); glVertex2f(320+0.5*sFontmen->w , 5);
		glTexCoord2f(1,1); glVertex2f(320+0.5*sFontmen->w, 5+ sFontmen->h);
		glTexCoord2f(0,1); glVertex2f(320-0.5*sFontmen->w, 5 + sFontmen->h);
	}
	glEnd();
	//TTF_CloseFont(font); put this in the destructor
	SDL_FreeSurface(sFontmen);
//Render ..
	if(end!=count)
	{
			color={0,0,0,0};
			sFontmen=TTF_RenderText_Blended(fontsel, "...", color);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, sFontmen->w, sFontmen->h, 0, GL_BGRA, 	GL_UNSIGNED_BYTE, sFontmen->pixels);
	glBegin(GL_QUADS);
	{
		glTexCoord2f(0,0); glVertex2f(100, 45);
		glTexCoord2f(1,0); glVertex2f(100+sFontmen->w , 45);
		glTexCoord2f(1,1); glVertex2f(100+sFontmen->w, 45+ sFontmen->h);
		glTexCoord2f(0,1); glVertex2f(100, 45 + sFontmen->h);
	}
	glEnd();
	//TTF_CloseFont(font); put this in the destructor
	SDL_FreeSurface(sFontmen);
	}
	if(begin!=0 )
	{
			color={0,0,0,0};
			sFontmen=TTF_RenderText_Blended(fontsel, "...", color);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, sFontmen->w, sFontmen->h, 0, GL_BGRA, 	GL_UNSIGNED_BYTE, sFontmen->pixels);
	glBegin(GL_QUADS);
	{
		glTexCoord2f(0,0); glVertex2f(100, 375);
		glTexCoord2f(1,0); glVertex2f(100+sFontmen->w , 375);
		glTexCoord2f(1,1); glVertex2f(100+sFontmen->w, 375+ sFontmen->h);
		glTexCoord2f(0,1); glVertex2f(100, 375 + sFontmen->h);
	}
	glEnd();
	//TTF_CloseFont(font); put this in the destructor
	SDL_FreeSurface(sFontmen);
	}
	glDisable(GL_BLEND);
	glDepthMask(GL_TRUE);  // disable writes to Z-Buffer
	glEnable(GL_DEPTH_TEST);
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();
	glDeleteTextures(1, &texture);





	glPopMatrix();
SDL_GL_SwapBuffers();

        SDL_WaitEvent(&event);
        switch(event.type)
        {
            case SDL_QUIT:
				delete camera;
				TTF_CloseFont(fontsel);
				TTF_CloseFont(fontunsel);	
                SDL_Quit();
	
				//cout<<"test"<<endl;
			break;
			
			case SDL_KEYDOWN:
                switch (event.key.keysym.sym)

                {

                   case SDLK_ESCAPE:
					delete camera;
					TTF_CloseFont(fontsel);
					TTF_CloseFont(fontunsel);
                    SDL_Quit();
                    break;
					case SDLK_RETURN:
					//go=false;
					TTF_CloseFont(fontsel);
					TTF_CloseFont(fontunsel);
					TTF_CloseFont(fontinfo);
					return pDirenta[sel]->d_name;
					break;
					case SDLK_UP :
					sel++;
					if(sel>=count){
						sel=count-1;}
					else if(sel==end)
					{
						begin++;
						end++;
					}
					temp=path+"../Data/"+pDirenta[sel]->d_name+"/Init.txt";
					fileselect.open(temp.c_str());
					fileselect>>stemp;
					lineone=stemp;
					fileselect>>setw(6)>>stemp;
					lineone+=" : "+stemp+"		";
					fileselect>>setw(6)>>stemp;
					lineone+="dt : "+stemp+"ps";
	//				cout<<endl<<lineone<<endl;	
					fileselect>>setw(6)>>stemp;
					linetwo="It : "+stemp+"		";
					fileselect>>setw(6)>>stemp;
					linetwo+="T : "+stemp+"K";
		//			cout<<endl<<linetwo<<endl;	
					fileselect.close();
					break;
					case SDLK_DOWN :
					sel--;
					if(sel<0)
						sel=0;
					if(sel<begin)
					{
						begin--;
						end--;
					}
					temp=path+"../Data/"+pDirenta[sel]->d_name+"/Init.txt";
					fileselect.open(temp.c_str());
					fileselect>>stemp;
					lineone=stemp;
					fileselect>>setw(6)>>stemp;
					lineone+=" : "+stemp+"		";
					fileselect>>setw(6)>>stemp;
					lineone+="dt : "+stemp+"ps";
		//			cout<<endl<<lineone<<endl;	
					fileselect>>setw(6)>>stemp;
					linetwo="It : "+stemp+"		";
					fileselect>>setw(6)>>stemp;
					linetwo+="T : "+stemp+"K";
				//	cout<<endl<<linetwo<<endl;	
					fileselect.close();
					break;
				}
			break;
        }


    }
	TTF_CloseFont(fontsel);
	TTF_CloseFont(fontunsel);
	TTF_CloseFont(fontinfo);

}


void quit()
{
    delete camera;
	TTF_Quit();
    SDL_Quit();
}
