# Bluebird - CatBird

<p align='center'>Welcome into the wonderful universe of molecular dynamic simulation!</p>


## Dependencies

sdl sdl2 sdl_image sdl_ttf cpp make glu

## Installation & Compilation

```
~$ git clone https://gitlab.com/plut0n/bluebird.git
~$ cd bluebird/
~$ make -j2
```

## Usage

``` ~/bluebird $ ./Bluebird/main ```

- 1.Initialise a system
    - choose the type of molecules
	- choose the number of molecules
	- choose the density (i.e. 1e23)
	- choose the system temperature
	- choose either you want to simulate a gas or a solid <br>    solid case : choose between the different crystal structures

- 2.Show your system

- 3.Process the system
	- choose the number of iterations (each iteration will caclute the temperature, the pression and each molecule position, speed and acceleration)
	- choose the time between each iteration (i.e. 1e3)
	- choose the number of steps between a rescale (non-free system)
	- choose the number of steps 

- 4.Visualise the simulation
	- ```~bluebird $ ./CatBird/CatBird ```
	- choose the folder corresponding to your simulation
	- simulation shortkeys:
        - spacebar : pause the simulation
	    - a : visualise via a coloration the acceleration (color of each molecule will show its acceleration )
	    - v : visualise via a coloration the speed
	    - n : non-color mode
	    - keyboard arrow : change the camera angle
	    - Page up/down : change the coloration's scale of acceleration/speed
	    - 1,2,3 : choose either to display or not the graph 1 2 3 
	    - g : display/hide graphs
	    - r : divide molecule's radius per 20
	    - esc : back to folder (simulation ) choice | press esc twice to exit CatBird

## Screenshots

No color mode:
<p align='center'><img src='https://gitlab.com/plut0n/Bluebird/raw/master/Demo/CatBird1.png'</p>

Coloration in speed:
<p align='center'><img src='https://gitlab.com/plut0n/Bluebird/raw/master/Demo/CatBird2.png'</p>

Coloration in acceleration:
<p align='center'><img src='https://gitlab.com/plut0n/Bluebird/raw/master/Demo/CatBird3.png'</p>