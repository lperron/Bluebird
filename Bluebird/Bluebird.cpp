#include<iostream>
#include<cstring>
#include<cmath>
#include<ctime>
#include<cstdlib>
#include<cstdbool>
#include<random>
#include<fstream>
#include<iomanip>
#include <sstream>
#include<sys/stat.h>
#include "Bluebird.h"

using namespace std;

/*******************************Box******************************/
 
Box::Box(string path)
{
    srand(time(NULL));
    pwd=path;
    //Initialisation : Number of Particule
    cout<<endl<<"Wich type of Clooney do you to use for your own pleasure : "<<endl;
    fstream mol;
    mol.open(pwd+"./molecule.txt");
    double Mass;
    if(mol.good())
    {
	    cout<<"N°:  Mol.Name  :     epsilone (eV)        sigma (m^(-12))      mass (eV/c^2)"<<endl;
	    string tmp;
	    int select=1;
	    while(!mol.eof()){
		cout<<endl<<select<<": ";
		mol>>tmp;
		cout<<tmp<<"  :  ";
		mol>>tmp;
		cout<<tmp<<"    ";
		mol>>tmp;
		cout<<tmp<<"    ";
		mol>>tmp;
		cout<<tmp;
		mol>>tmp;
		select++;
	    }
	    cout<<endl<<"Your choice sir : ";
	    cin>>select;
	    mol.clear();
	    mol.seekg(0,ios::beg);
	    for(int i=0;i<5*(select-1);i++)
		    mol>>tmp;
	    mol>>molname;
	    mol>>epsi;
	    mol>>sigma;
	    mol>>Mass;
	    cout<<endl<<"epsi : "<<epsi<<"       sigma : "<<sigma<<"        mass : "<<Mass<<endl;
    }
    else
    {
	    cerr<<"Can't find molecule.txt in "<<pwd<<", please set your own value"<<endl;
	    cout<<endl<<"Lennard Jones Potential minimum value (espilone in eV) : ";
	    cin>>epsi;
	    cout<<endl<<"Lennard Jones Potential zero distance (sigma in eV) : ";
	    cin>>sigma;
    }
    cout<<endl<<"How much do you like Clooney : ";
    do{
        cin>>N;
        if(N<1)
            cout<<"N must be a positive integer !";
    }while(N<1);
    //Initialisation : Molecular density
    cout<<"Molecular density you'd like (in e27/m^3 : ~ 0.02504 ideal gas / 33.36 water / 176.2 diamond ) : ";
    do{
        cin>>rho;
        if(rho<1e-27)
            cout<<"Density has to be > 0 !";
    }while(rho<1e-27);
    rho*=1e27;
    //Initialisation : System's initial Temperature
    cout<<"Initial System Temperature (K) : ";
    do{
        cin>>KelvinUser;
        if(Kelvin<0)
            cout<<"Initial Temperature has to be > 0 !";
    }while(Kelvin<0);
    Kelvin=KelvinUser;
    //Initialisation : Molecule's mass
    cout<<"State of matter : "<<endl<<"    1: Gas"<<endl<<"    2: Solid (crystal only)"<<endl;
    cin>>state;
    switch(state)
    {
     case 1 :
         {
             break;
         }
     case 2 :
         {
             fstream crystal;
             crystal.open(pwd+"./crystal_basis.txt");
             if(crystal.good())
             {
                 string tmp;
                 int k=1;
                 do{
                    crystal >> tmp;
                    cout<<endl<<k<<": "<<tmp;
                    crystal >> tmp;
                    cout<<" "<<tmp;
                    getline(crystal,tmp);
                    getline(crystal,tmp);
		    getline(crystal,tmp);
                    k++;
                 }while(!crystal.eof());
                 do{
                        cout<<endl<<endl<<"Choose a crystal structure : ";
                        cin>>cryst;
                 }while(cryst<1);
                 crystal.close();
             }
             else
                 cerr<<endl<<"Can't find crystal_basis.txt in "<<pwd<<" !"<<endl;
             break;
         }
    }
    //Box Size
    Lengh=pow(((double)N)/rho,1./3.);
    Size={Lengh,Lengh,Lengh};
    cout<<endl;
    
    //Molecular Distribution
    random_device rd; //Non deterministic random numbers
    mt19937_64 gen(rd()); //Mersenne generator for more accurate pseudo random (64bits)
    distribution = MaxwellBoltzmannGen(Mass);
    
    if(state == 1)
    {   
	Distrib=new Clooney[N];
        Vector tmp;
        bool ok;
        for(int i=0;i<N;i++)
        {
            do{
                ok=true;
                *(Distrib+i)=Clooney(Size,Mass,Kelvin,&gen,&distribution);
                //Physically, there's no sens of really close molecule (due to the repulsivity of the Lenard-Jones Potential when relative position are < sigma )
                for(int j=0;j<i;j++)
                {
                    tmp=(Distrib+i)->GetPos()-(Distrib+j)->GetPos();
                    if(tmp.norm() <= sigma)
                        ok=false; //We 'reconstruct' the molecule with a relativ postion to any over < sigma
                }
            }while(ok==false);   
        }
    }
    else
    {
        fstream crystal;
        crystal.open(pwd+"./crystal_basis.txt");
        string tmp; 
        int nbbase; 
        getline(crystal,tmp);
        for(int i=0;i<(3*(cryst-1));i++)
            getline(crystal,tmp);
        crystal >> nbbase; //number of atom providing the crystal's basis
		cout<<nbbase<<endl;
        int select;
	int M=0;
        while(M*M*M*nbbase<N)
		M++;
	do{
            cout<<endl<<"To fit perfectly the space, number of particule can be set to :"<<endl<<"    1: "<<M*M*M*nbbase<<endl<<"    2: "<<(M-1)*(M-1)*(M-1)*nbbase<<endl;
            cin>>select;
            if(select==2)
                M-=1;
        }while(select < 1 || select > 2);
	N=M*M*M*nbbase;
	Distrib=new Clooney[N];
	double lattice = Lengh / (double)M;

        //Construction of the nbbase vector
        Vector Tmp(3);
        Vector* Basis=new Vector[nbbase];
        for(int i=0;i<nbbase;i++)
        {
            (Basis+i)->Redim(3);
            for(int j=0;j<3;j++)
                crystal >> Tmp(j);
            *(Basis+i)=Tmp;
        }
        int n=0;
        for(int x=0;x<M;x++)
            for(int y=0;y<M;y++)
                for(int z=0;z<M;z++)
                    for(int j=0;j<nbbase;j++)
                        if(n<N){
                                *(Distrib+n)=Clooney(Size,Mass,Kelvin,&gen,&distribution,*(Basis+j),x,y,z,lattice);
				n++;
                        }
    }
    //Initializing the Center of Mass
    CenterofMassVelo();
    KelvinAtT();
}

//Utilities
Vector Box::GetSize() const
{
	return Size;
}

Clooney* Box::GetDistrib() const
{
	return Distrib;
}

int Box::GetN() const
{
	return N;
}

string Box::GetPwd() const
{
    return pwd;
}

ostream & operator<< (ostream & out,  const Box  & v)
{
	int N=v.GetN();
    Vector Size=v.GetSize();
	out<<endl<<"Size : "<<Size<<endl<<"Distribution : "<<endl;
	Clooney* Distr=v.GetDistrib();
	for (int i = 0; i < N; i++)
		out<<(Distr+i)->GetPos()<<endl;
    out<<endl<<"Speed : "<<endl;
    for (int i = 0; i < N; i++)
        out<<(Distr+i)->GetSpeed()<<endl;
  return out;
}


const Box & Box::operator= (const Box & v)
{
	N=v.GetN();
	rho=v.GetRho();
        pwd=v.GetPwd();
	Size=v.GetSize();
	Distrib=v.GetDistrib();
	sigma=v.GetSigma();
	epsi=v.GetEpsi();
	molname=v.GetMolName();
	KelvinUser=v.GetKU();
  return (*this); // Giving back the affectation's result
}

//Functions and other

normal_distribution<double> Box::MaxwellBoltzmannGen(double Mass)
{
    const double Kb = 0.00008617; //Boltzmann constant in eV/K
    const double c = 2.9979e8; //light celerity
    double sigm=sqrt(Kb*Kelvin/Mass)*c; //Sigma is the same dimension of v. In order to have the speed in m/s, we multiply sigma by c
    cout<<endl<<"Sigma : "<<sigm<<endl;
    normal_distribution<double> distribution(0.,sigm);
    return distribution;
}

void Box::CenterofMassVelo()
{
    for(int i=0;i<N;i++)
        CoMV+=(Distrib+i)->GetSpeed();
    CoMV/=N; //center of mass velocity is now defined
    for(int i=0;i<N;i++)
        (Distrib+i)->SetSpeed((Distrib+i)->GetSpeed()-CoMV); //We substract the CoM to the Speed of each Molecule in order
}

void Box::RescaleSpeed()
{
    double SquareSum=0;
    for(int i=0;i<N;i++)
        SquareSum+=(Distrib+i)->GetSpeed()*(Distrib+i)->GetSpeed();
    double fact=sqrt(3.*((double)N-1.)*0.00008617*KelvinUser*2.9979e8*2.9979e8/((Distrib+0)->GetMass()*SquareSum)); //here's the factor that scale the velocities such that the Temperature at time t is the one desired
    for(int i=0;i<N;i++)
        (Distrib+i)->SetSpeed((Distrib+i)->GetSpeed()*fact);
}

void Box::LJpotAcc()
{
    Vector tmp={0,0,0};
    Vector tmp2={0,0,0};
    for(int i=0;i<N;i++)
        (Distrib+i)->SetAcc(tmp);
    double LJ;
    double pos2; //The scalare = position square
    Vector posrelativ={0,0,0}; //Vector storing distance between two differents molecules
    for(int i=0;i<N-1;i++)
    {
        for(int j=i+1;j<N;j++)
        {
            posrelativ=(Distrib+i)->GetPos()-(Distrib+j)->GetPos();
            for(int k=0;k<3;k++)
            if(posrelativ(k) > 0.5*Lengh)
            {
                if(posrelativ(k)>0)
                    posrelativ(k)-=Lengh;
                else
                    posrelativ(k)+=Lengh;
            }
            pos2=posrelativ*posrelativ;
            LJ = 24*epsi*(2*pow(sigma,12)*pow(pos2,-7)-pow(sigma,6)*pow(pos2,-4)); //derivative of the Lennard Jones potential calculation
            tmp=(Distrib+i)->GetAcc()+posrelativ*LJ/(Distrib+i)->GetMass(); //Newton's equation of motion
            tmp2=(Distrib+j)->GetAcc()-posrelativ*LJ/(Distrib+j)->GetMass();
            (Distrib+i)->SetAcc(tmp);
            (Distrib+j)->SetAcc(tmp2);            
	    }
    }
}

void Box::Verlet()
{
    LJpotAcc(); //Acc computation
    for(int i=0;i<N;i++)
    {
        (Distrib+i)->SetPos((Distrib+i)->GetPos()+(Distrib+i)->GetSpeed()*stepdt+(Distrib+i)->GetAcc()*.5*stepdt*stepdt); //Verlet : position computation
	(Distrib+i)->PacmanPosition(Size); //Periodic Boundary Condition are set
        (Distrib+i)->SetSpeed((Distrib+i)->GetSpeed()+(Distrib+i)->GetAcc()*.5*stepdt);   // Verlet : Speed computation 1st step
    }
    LJpotAcc(); //Computation of accelartion at t+dt
    for(int i=0;i<N;i++)
        (Distrib+i)->SetSpeed((Distrib+i)->GetSpeed()+(Distrib+i)->GetAcc()*.5*stepdt); //Velert : Speed final computation (with the acc at t +dt term)
}

void Box::Test(){
    cout<<endl<<"25 :"<<(Distrib+25)->GetPos()<<endl;
    cout<<"Step dt : ";
    cin>>stepdt;
    Verlet();
    //Verlet();
    //(Distrib+25)->SetPos((Distrib+25)->GetPos()+(Distrib+25)->GetSpeed()*stepdt+(Distrib+25)->GetAcc()*.5*stepdt*stepdt);

    cout<<endl<<"25 set : "<<(Distrib+25)->GetPos()<<endl;
}

void Box::KelvinAtT()
{
    Kelvin=0;
    for(int i=0;i<N;i++)
        Kelvin+=((Distrib+i)->GetSpeed()*(Distrib+i)->GetSpeed());
    Kelvin/=(3.*(N-1.)*0.00008617*2.9979e8*2.9979e8);
    Kelvin*=(Distrib)->GetMass();
}

void Box::Pressure()
{
    double W; //virial term
    for(int i=0;i<N-1;i++)
        for(int j=i;j<N;j++)
        {
            W=((Distrib+i)->GetAcc()-(Distrib+j)->GetAcc())*(((Distrib+i))->GetPos()-(Distrib+j)->GetPos()); //sum on all particule of: force between each other * relative position
        }
    W=abs(W);
    W/=(Distrib+1)->GetMass(); //because all molecules have the same mass for now (improvement?)
    W/=N; //takes the mean fores.relative position
    double v=pow(Size(1),3);
    //cout<<endl<<"V : "<<v;
    W/=3.*v; //divided by the system volume
    Pres=rho*0.00008617*Kelvin+W; //P=(N/V)*kb*T+W/v
    Pres*=1.609e-19; //set Pression from eV.m^-3 to Pa (= J.m^-3)
}

void Box::RadialDisF()
{
	int nm=0; //number of molecule inside the sphere
	int size=0; //size tab, a voir si math possible
	double Mnm=0; //mean number of particule inside sphere
	int tmp=(Size(1)*sqrt(3)/stepdr);
	rdf=new double[tmp]; //as a function of distance
	for(int i=0;i<N-1;i++)
		for(int j=i;j<N;j++)
			size++;
	double posr[size];
	Vector posrelativ;
	size=0;
	for(int i=0;i<N-1;i++) //zueb a faire en fonction annexe
		for(int j=i;j<N;j++)
		{
			posrelativ=(Distrib+i)->GetPos()-(Distrib+j)->GetPos();
            		for(int k=0;k<3;k++)
            			if(posrelativ(k) > 0.5*Lengh)
            			{			
                			if(posrelativ(k)>0)
                			    posrelativ(k)-=Lengh;
               				 else
                			    posrelativ(k)+=Lengh;
            			}
			posr[size]=posrelativ.norm();
			size++;
		}
	for(int r=0;r<tmp;r++)
	{
		for(int i=0;i<size;i++)
		{
			if(posr[i]-(r*stepdr) <= stepdr && posr[i]-(r*stepdr) > 0)
				nm++; //number of particule inside shell
		}
		Mnm+=((double)nm/(double)N); //mean number of particule inside the shell
		rdf[r]=3.*Mnm/(pow(r*stepdr+stepdr,3)-pow(r*stepdr,3));
		rdf[r]/=rho*4.*M_PI;
		nm=0;
	}
}

void Box::ComputeAll()
{
    cout<<"Number of iterations : ";
    do{
        cin>>Ite;
        if(Ite<1)
            cout<<"Number of iteration need to be at least > 1";
    }while(Ite<1);
    cout<<endl<<"Temporal step size (pico s) : ";
    cin>>stepdt;
    int ft,Rescale;
    do{
    	cout<<endl<<"What kind of system would you like :"<<endl<<"    1.Free system"<<endl<<"    2.With a Thermostat"<<endl<<"Your choice : ";
    	cin>>ft;
    	if(ft==2)
	{
		do{
			cout<<endl<<"Number of step between speed rescaling (thermostat) : ";
			cin>>Rescale;
			if(Rescale<1)
				cout<<"At least 1 step between each rescaling is mandatory!"<<endl;
		}while(Rescale<1);
	}
    }while(ft <1 || ft>2);
    cout<<endl<<"Radial distribution step size (% of Grand Diagonal of BoxSize) : ";
    do{
    	cin>>stepdr;
	if(stepdr<0 || stepdr>=100)
		cout<<"No-sense, please be logical";
    }while(stepdr<0 || stepdr>=100);
    stepdr*=(GetSizeK(1)*sqrt(3))/100.;
    cout<<endl<<"Number of step between data output file (3D-Simulation) : ";
    do{
        cin>>IteWrite;
        if(IteWrite<1)
            cout<<"At least 1 step between data output file";
    }while(IteWrite<1);
    stepdt/=1e12;
    cout<<endl<<"Number between data output file with radial distribution function : ";
    do{
	cin>>steprdf;
	if(steprdf<1)
		cout<<"At least one step between rdf data output file";
    }while(steprdf<1);
    steprdf*=IteWrite;
    cout<<endl;
    double time=0;
    KelvinAtT();
    WriteInit();
    int CatBird=0;
    cout<<"Would you like to see the simualtion while calculating it : "<<endl<<"    1. Yes"<<endl<<"    2. No"<<endl<<"What's your whish : ";
    do{
	cin>>CatBird;
	if(CatBird ==1)
	{
		string tmp=pwd+"../CatBird/CatBird &";
		system(tmp.c_str());
	}
    }while(CatBird <1 || CatBird >2);
    if(ft ==1)
    {
	begintime = clock();
    	for(int i=0;i<Ite;i++)
    	{
	    Progress(i); //only to know approximately what percentage of simulation is done/left
    	    //if(i % IteWrite == 0)    
	    //	WriteStep(i);
	    Verlet();
	    time+=stepdt;
	    KelvinAtT();
	    Pressure();
	    if(i%IteWrite == 0)
		    WriteStep(i);
	    if(i%steprdf == 0)
	    	    WriteRdf(i);
    	}
    }
    else
    {
	    RescaleSpeed();
	    begintime=clock();
	    for(int i=0;i<Ite;i++)
	    {
		Progress(i);
		//if(i % IteWrite ==0)
		//	WriteStep(i);
		Verlet();
		KelvinAtT();
		Pressure();
		if(i % Rescale == 0)
			RescaleSpeed();
		if(i % IteWrite == 0)
			WriteStep(i);
		if(i % steprdf == 0 )
			WriteRdf(i);
	    }
    }
    ComputationInfo();
}

void Box::Progress(int done)
{
	cout<<"[";
	double progress = ((double)done/(double)(Ite));
	int pos = 70*progress;
	for(int i=0;i<70;i++)
	{
		if(i<pos)
			cout<<"=";
		else if(i==pos)
			cout<<">";
		else
			cout<<" ";
	}
	cout<<"] "<<setw(3)<<progress*100.0<<" %\r";
	cout.flush();

}

void Box::ComputationInfo()
{
	cout<<endl<<endl<<"Computation Information :"<<endl;
	cout<<Ite<<" iteration realised in "<<double(clock()-begintime)/CLOCKS_PER_SEC<<"s";
	cout<<endl<<endl;
}

void Box::WriteInit()
{
    Vector tmp;
    dir = pwd+"../Data/";
    string pb;
    cout<<"Name of your Simulation (will be stored in ../Data/YourSimulationName) : ";
    cin>>pb;
    cout<<endl;
    dir+=pb;dir+="/";
    //dir=ssdir.str();
    string tmp2="mkdir "+dir;
    system(tmp2.c_str());
    string file=dir+"Init.txt";
    ofstream init(file);
    init<<molname<<" "<<N<<" "<<stepdt*IteWrite*1e12<<" "<<Ite/IteWrite<<" "<<setw(25)<<setprecision(20)<<Kelvin<<" "<<" "<<Size(0)<<" "<<Size(1)<<" "<<Size(2)<<" "<<steprdf/IteWrite<<" "<<Size(0)*sqrt(3)/stepdr<<" ";
    for(int i=0;i<N;i++)
    {
        tmp=(Distrib+i)->GetPos();
        for(int j=0;j<3;j++)
            init<<tmp(j)<<" ";
		init<< (Distrib+i)->GetSpeed().norm()<<" "<<(Distrib+i)->GetAcc().norm()<<" ";
    }
}

void Box::WriteStep(int st)
{
    Vector tmp;
	stringstream temp;
	temp<<(st/IteWrite);
    string file=dir+("Step")+temp.str()+(".txt");
    ofstream fstep(file);
    fstep<<Kelvin<<" "<<Pres<<" ";
    for(int i=0;i<N;i++)
    {
        tmp=(Distrib+i)->GetPos();
        for(int j=0;j<3;j++)
            fstep<<tmp(j)<<" ";
		fstep<< (Distrib+i)->GetSpeed().norm()<<" "<<(Distrib+i)->GetAcc().norm()<<" ";
    }
}

void Box::WriteRdf(int st)
{
	RadialDisF();
	stringstream tmp;
	tmp<<(st/(IteWrite*steprdf));
	string file=dir+("Rdf")+tmp.str()+(".txt");
	ofstream frdf(file);
	for(int r=0;r<Size(1)*sqrt(3)/stepdr;r++)
		frdf<<r*stepdr<<" "<<rdf[r]<<" ";	
}

/*******************************Clooney****************************/

Clooney::Clooney(Vector Size,double Mass,double Kelvin,mt19937_64* gen, normal_distribution<double>* distribution)
{
    mass=Mass;
	position.Redim(3);
	for(int i=0;i<3;i++)
	{
		position(i)=((double)rand()/RAND_MAX) *Size(i); //random distribution for molecule's position
	}
	speed.Redim(3);
    MaxwellBoltzmannDist(gen, distribution); //Maxwell-Boltzmann distribution for molecule's speed
}

Clooney::Clooney(Vector Size,double Mass, double Kelvin,mt19937_64* gen, normal_distribution<double>* distribution,Vector Basis,int x, int y, int z, double a)
{
    mass=Mass;
    position.Redim(3);
    position(0)=Basis(0)+x;
    position(1)=Basis(1)+y;
    position(2)=Basis(2)+z;
    position*=a;
    speed.Redim(3);
    MaxwellBoltzmannDist(gen, distribution);
}

void Clooney::PacmanPosition(Vector Size){
		for(int k=0;k<3;k++)
		{
			if(position(k)>Size(k))
			position(k)=position(k)-Size(k);
			else if(position(k)<0)
			position(k)+=Size(k);
		}
}

void Clooney::MaxwellBoltzmannDist(mt19937_64* gen, normal_distribution<double>* distribution){
    normal_distribution<double> distrib= *distribution;
    for(int i=0;i<3;i++)
        speed(i)=distrib(*gen);
}

Vector Clooney::GetPos() const
{
	return position;
}

Vector Clooney::GetSpeed() const
{
	return speed;
}

Vector Clooney::GetAcc() const
{
    return acc;
}

void Clooney::SetPos(Vector Pos)
{
    position=Pos;
}

void Clooney::SetPosK(double value, int K)
{
    position(K)=value;
}

void Clooney::SetSpeed(Vector Speed)
{
    speed=Speed;
}

void Clooney::SetAcc(Vector Acc)
{
    acc=Acc;
}
