#ifndef _BLUEBIRD_H_
#define _BLUEBIRD_H_


#include "Vector.h"
#include <random>
#include <ctime>

using namespace std;


class Clooney //molecules propreties
{
    protected :
    Vector position; //molecule's position
	Vector speed; //molecule's speed
    Vector acc={0,0,0}; //molecule's acceleration
    double mass; //molecule's mass
	bool outofthebox=false;

	public :
	Vector GetPos() const;
	Vector GetSpeed() const;
    Vector GetAcc() const;
    double GetMass() const{return mass;};
    void SetPos(Vector Pos);
    void SetPosK(double value, int K);
    void SetSpeed(Vector Speed);
    void SetAcc(Vector Acc);
	Clooney(Vector Size,double Mass, double Kelvin,mt19937_64* gen, normal_distribution<double>* distribution); //Initialize each molecule with a random position and a MaxwellBotlzmann distribution for the speed (we consider the system more or less as an ideal gas)
    Clooney(Vector Size,double Mass, double Kelvin,mt19937_64* gen, normal_distribution<double>* distribution,Vector Basis,int x,int y,int z,double a);//Initialize each molecule as the constructor before but with determine position
    void MaxwellBoltzmannDist(mt19937_64* gen, normal_distribution<double>* distribution);
	void PacmanPosition(Vector Size); //If a molecule goes out of the box, its position will be set as if it re-enter the the box from the other side
	Clooney(){};   //It's so ugly, we need to correct this, it's used to create the clooney dynamic array
	~Clooney() {}
};


class Box
{
	protected:
    string pwd;
    string dir; //Where all the file needed for the simulation will be stored
    string molname;
	Vector Size;
	Clooney* Distrib;  //Clooney's distribution
	int N;  //How many Clooney do you want?
    int state; //state of the matter composing the system
    int cryst; //crystal strucutre type if needed 
    double mass;
	double Kelvin; //System temperature
	double KelvinUser; //User wanted temperature
    double rho; //Molecular density
    double Lengh; //Box Length
    double Pres; //System Pressure given by the virial theorem
    double stepdr; //Step for radial distribution function
    int steprdf; //number of iterations between those the radial distribution function is calculated
    double* rdf; //Radial distribution function
    int Ite; //number of stepdt
    int IteWrite; //number of stepdt between each data file written (for the 3D-Simulation)
    clock_t begintime; //time at wich the simulation is launch
    double stepdt; //define the smallest dt of the system
    double epsi=1,sigma=1; //the two parameter of the lennard jones potential
    normal_distribution<double> distribution; //MaxwellBoltzmann distribution
    Vector CoMV={0,0,0}; //Center of Mass

	public:
	Vector GetSize() const;  //simply return the size of the box
	double GetSizeK(int K) const{return Size(K);};
	Clooney* GetDistrib() const;
	int GetN() const; //number of particule
    string GetPwd() const;
    double GetSigma() const{return sigma;};
    double GetEpsi() const{return epsi;};
    string GetMolName() const{return molname;};
    double GetKU() const{return KelvinUser;};
    double GetRho() const{return rho;};
    normal_distribution<double> MaxwellBoltzmannGen(double Mass); //Maxwell Boltzann distribution generation
    void CenterofMassVelo(); //Set the Center of Mass Velocities and rearanged all molecule's in order to have the total system's momentum = 0
    void RescaleSpeed();
    void LJpotAcc();
    void Verlet();
    void KelvinAtT();
    void Pressure();
    void RadialDisF();
    void ComputeAll();
    void Progress(int done);
    void ComputationInfo();
    void WriteStep(int st);
    void WriteInit();
    void WriteRdf(int st);
    
    void Test();

	friend ostream & operator<< (ostream & out,const  Box & v);
	const Box & operator= (const Box & v);
	
    Box();
    Box(string path);
	Box(int){}; //SOOOO UUUGGGLYYYYY
	//we need another constructor for file generated distribution
	~Box() {}
};

#endif
