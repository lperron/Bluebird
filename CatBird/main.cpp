#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_syswm.h> 
#include <GL/gl.h>
#include <GL/glu.h>
#include <sys/types.h>
#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <fstream>
#include <ctime>
#include <cstring>
#include<dirent.h>
#include <cmath>
#include "box.gouda"
#include "cam.gouda"
#include "sdlglutils.gouda"

#define FPS 30

using namespace std;

void DrawGL(Box * b);	//this function draws all
void quit();	//explicit
bool poke=false;	//easter egg 
string menu(string spath);	//the menu function, return the name of the simu
Cam* camera=NULL;	//our camera
SDL_Surface * screen=NULL;	//our window
float golden=(1+sqrt(5))/2.;	//it can be useful
int height=480;	//size of the window
int width=640;//
string path;	//path to the program location

int main(int argc, char *argv[])
{
	string temp(argv[0]);	//argv[0] contains the command used to launch Catbird
	path=temp.substr(0,temp.length()-7);//ex : ./CatBird/CatBird -CatBird = ./CatBird/    useful to know where are the data
	bool pause=false;	//explicit
	if(argc>1)		//additionnal command, to activate the easter egg, use --poke
	{
	if(strcmp(argv[1],"--poke")==0) 
		poke=true;			
	else
      poke=false;
	}
	srand(time(NULL));

    SDL_Event event;	//a structure containing all information about SDL event. For more informations : http://sdl.beuc.net/sdl.wiki/SDL_Event
	const Uint32 time_per_frame = 1000/FPS;	
    SDL_Init(SDL_INIT_VIDEO);	
    atexit(quit);
	temp=path+"feather.png";	//logo
	const char * cpath=temp.c_str();
    SDL_WM_SetCaption("CatBird", NULL);	//our window informations
	SDL_WM_SetIcon(IMG_Load(cpath), NULL);
	TTF_Init();   	//to use the TTF Lib
    Uint32 last_time = SDL_GetTicks();	//some time stuffs, useful to set the FPS
    Uint32 current_time,elapsed_time;
    Uint32 start_time, stop_time;
	height=480;
	width=640;
for(;;){	//infinite loop, but we had an exit function, so it's OK
	
	SDL_FreeSurface(screen);	//because we are in a loop and if switch from simu to menu mode, we need to free the screen
	screen = SDL_SetVideoMode(width, height, 32, SDL_OPENGL );	//simple window, non-resizable, video mode used for the menu
	string data=menu(path);//cout<<endl<<data<<endl;	//here we launch the menu function, returning the path to the sime
	SDL_FreeSurface(screen);	//'cause we change the mode
	screen = SDL_SetVideoMode(width, height, 32, SDL_OPENGL | SDL_RESIZABLE| SDL_ASYNCBLIT );	//video mode used for the simu
	glMatrixMode( GL_MODELVIEW );	//opengl
	glLoadIdentity();	//there will be many things like this in the code
   	glMatrixMode( GL_PROJECTION );	//
	glLoadIdentity();	//
	gluPerspective(70,(double)width/height,0.001,1000);	//we set the aspect of the perspective (fov, apsect, znear,zfar)
    glEnable(GL_DEPTH_TEST);	//it's better if we want to do 3D...
	Box b(path,poke,data,width,height);	//We create our box, so, please see box.cpp and box.gouda
	   b.DrawBox();	//we draw it

	bool key[8]={0,0,0,0,0,0,0,0};	//boolean array to know some input states (key pressed but not released)
	int cmode=0;		//color mode 0->none (all in red) 2->acceleration 1->velocity
	double maxa=1e-15;	//calibration of the acceleration scale
	double maxv=300;// same for the velocity scale

		int x=b.getx();	//size of the box
	int y=b.gety();	//same, other axis
	int z=b.getz();	//I think you know..
	camera = new Cam(sqrt((x*x+y*y+z*z)/golden));	//we create our cam, the argument is the distance from the middle of the box
    camera->setScrollSensivity(0.4);	//explicit
	SDL_EnableUNICODE(1);	//I'm using an azerty keyboard, unicode is universal !!! so unicode inputs work well for me, and for other keyboard layouts
	int first=0;	//it's juste a variable used to know if we have already done two loop in the next do while
	bool bexit=false;	//boolean to quit the simu and to return to the menu
    do
    {
	if(first<2)		//this trick avoid a bug when moving the menu window
	{				//laziness avoid me to find the real problem
    SDL_Event sdlevent;
    sdlevent.type = SDL_VIDEORESIZE;
    sdlevent.resize.h = height;	
	sdlevent.resize.w = width;
    SDL_PushEvent(&sdlevent);
	first++;
	}
		bexit=false;
        start_time = SDL_GetTicks();
        while (SDL_PollEvent(&event))	
        {

            switch(event.type)
            {
                case SDL_QUIT:	//The little cross on the top of the window ->quit the program
				
                exit(0);
                break;
                case SDL_MOUSEMOTION:	//here we manage the mouse events
		
                camera->mousemotion(event.motion);	//we move the cam according to the event (if the left button is down)
                break;
                case SDL_MOUSEBUTTONUP:	
                case SDL_MOUSEBUTTONDOWN:	
				camera->mouseclick(event.button);	//to know if the left button is down
				if(event.button.button == SDL_BUTTON_RIGHT and event.type == SDL_MOUSEBUTTONDOWN)	//right click change the color mode
				{
					cmode++;
					if(cmode==3)	//there is only 3 mode
						cmode=0;
					b.setcolormode(cmode);	//we change the color mode variable of the box
				}
                break;
               case SDL_KEYDOWN:
				b.selectgraphon(event.key);	//THIS function manage the activation/desactivation of the graph (using unicode characters)
                switch (event.key.keysym.sym)	//standard keyboard inputs	see : https://wiki.libsdl.org/SDL_Keycode

                {
                    case SDLK_SPACE:
						b.pauseevent();	//pause/play
                    break;
                    case SDLK_ESCAPE:	
                  //  exit(0);
					bexit=true;	//we return to the menu
                    break;
					case SDLK_UP :
						key[0]=true;	//camera movement
					break;
					case SDLK_DOWN :
						key[1]=true;	//same
					break;
					case SDLK_RIGHT :
						key[2]=true;	//same
					break;
					case SDLK_LEFT :	//and...same
						key[3]=true;
					break;
					case SDLK_KP_PLUS :	//zoom
						key[4]=true;
					break;
					case SDLK_KP_MINUS :	//de-zoom
						key[5]=true;
					break;
					case SDLK_a :
						b.setcolormode(2);	//colormode = acceleration
						cmode=2;
					break;
					case SDLK_v :
						b.setcolormode(1);	//velocity
						cmode=1;
					break;
					case SDLK_n :
						b.setcolormode(0);	//none
						cmode=0;
					break;
					case SDLK_PAGEDOWN :	//to adjust the scale of the selected color mode
						key[6]=true;
					break;
					case SDLK_PAGEUP :	//same
						key[7]=true;
					break;
					case SDLK_r :	
						b.SwitchRMode();	//this function reduces the size of the clooneys, not to the real size (toooo small) but it's sufficient to get an idea!
						break;
					case SDLK_g :	//all the graph on/off
						b.switchgraphon();
						break;
                }

                break;
				case SDL_KEYUP:	//when the user release a key
				switch (event.key.keysym.sym)

                {
					case SDLK_UP :
						key[0]=false;
					break;
					case SDLK_DOWN :
						key[1]=false;
					break;
					case SDLK_RIGHT :
						key[2]=false;
					break;
					case SDLK_LEFT :
						key[3]=false;
					break;
					case SDLK_KP_PLUS :
						key[4]=false;
					break;
					case SDLK_KP_MINUS :
						key[5]=false;
					break;
					case SDLK_PAGEDOWN :
						key[6]=false;
					break;
					case SDLK_PAGEUP :
						key[7]=false;
					break;
                }
				break;
				case SDL_VIDEORESIZE:	//http://sdl.beuc.net/sdl.wiki/SDL_ResizeEvent

                width = event.resize.w; // <- largeur
                height = event.resize.h; // <- hauteur	
				if(height<480)	//min size, not working as I want
				{
						height=480;
				}
				 if(width<640)
				{
					width=640;
				}
				{
					SDL_FreeSurface(screen);	//cause the window size is changing
          		 	screen = SDL_SetVideoMode(width ,height,32,SDL_OPENGL | SDL_RESIZABLE | SDL_ASYNCBLIT);	//with the new size
					b.Setwh(width,height);	//we tell it to the box
					glViewport(0,0,width,height);	//we change this with new size
   					glMatrixMode( GL_PROJECTION );	
					glLoadIdentity();
					gluPerspective(70,(double)width/height,0.001,1000);	//and the perspective too
				}
				break;
            }
        }
		if(key[0]==true)	//all these if are used to know if a key is pressed
			camera->uparrow();
		if(key[1]==true)
			camera->downarrow();
		if(key[2]==true)
			camera->leftarrow();
		if(key[3]==true)
			camera->rightarrow();
		if(key[4]==true)
			camera->plus();
		if(key[5]==true)
			camera->minus();

		if(key[6]==true)	//scale adjustement	//minus
		{
			if(cmode==2)	//acceleration
			{
				maxa/=1.1;
				if(maxa<1e-13)
					maxa=1e-13;		//min, I need to find a good value
				b.setmaxa(maxa);
			}
			if(cmode==1)		//velocity
			{
				maxv-=10;
				if(maxv<1)
					maxv=1;
				b.setmaxv(maxv);
			}
		}
		if(key[7]==true)	//same but plus
		{
			if(cmode==2)
			{
				maxa*=1.1;
				b.setmaxa(maxa);
			}
			if(cmode==1)
			{
				maxv+=10;
				b.setmaxv(maxv);
			}
		}		
		
        current_time = SDL_GetTicks();
        elapsed_time = current_time - last_time;
        last_time = current_time;
		DrawGL(&b);	//this function draw all the things we need
        elapsed_time = SDL_GetTicks() - start_time;
        stop_time = SDL_GetTicks();
		if((stop_time - last_time) < time_per_frame)
        {
            SDL_Delay(time_per_frame - (stop_time - last_time));	//simple delay, we just wait to stay at 30FPS, you can change the FPS value if you want to.
        }


   	}while(bexit==false);	//to go to the menu
	delete camera;
	}
    return 0;
}	//pfiouu

void DrawGL(Box * b)	//all the simu
{
	int color[3]={255,0,0};	//red
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );//we clear all
    glMatrixMode( GL_MODELVIEW );	//matrix stuff
    glLoadIdentity( );	//same
	camera->gogogo();	//we move our camera where the user want it
	glTranslated(-b->getx()/2.,-b->gety()/2.,-b->getz()/2.);	//we translate the scene because in open gl the middle of the screen is 0 0 0, but not in Bluebird
	b->DrawBox();	//we draw the box
	//cout<<endl<<"1"<<endl;
	b->DrawClooneys();	//and the Clooneys
	//cout<<endl<<"2"<<endl;
	glTranslated(b->getx()/2.,b->gety()/2.,b->getz()/2.);	//we undo our translation
	//cout<<endl<<"3"<<endl;
	b->HUD();	//all the 2D objects like text and graph
	//cout<<endl<<"4"<<endl;
	b->NextStep();	//we load the next file
	//cout<<endl<<"5"<<endl;
   SDL_GL_SwapBuffers();	//rendering of the scene.
}

string menu(string path)	//ja, the menu
{
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );	//we clear all
	glEnable(GL_COLOR_MATERIAL);	//we need to set some colors
	string temppath=path+"../Data/";	//where are stored the simu
	string stemp;	//i think i used this string sometimes
	string lineone;	//first info line
	string linetwo;	//second info line
	struct dirent *pDirent;	//all dirent things are used to explore the data directory; and the implementation is standard
	DIR *pDir;
	pDir=opendir(temppath.c_str());
	int count=0;	//number of sub directories
        while ((pDirent = readdir(pDir)) != NULL) {	//to get the number of sub-directories
           if ((strchr(pDirent->d_name, '.')) == NULL)
				count++;
        }
		
   		 closedir(pDir);
		pDir=opendir(temppath.c_str());
		struct dirent *pDirenta[count];	//now we can create an array of dirent to store all the sub-d
		int j=0;
		while ((pDirent = readdir(pDir)) != NULL) {
           if ((strchr(pDirent->d_name, '.')) == NULL)	//if there is a dot, i assume it's not a directory, so no dot when you're naming a simu in bluebird please.
				{pDirenta[j]=pDirent;j++;}
        }
	string temp=path+"../Data/"+pDirenta[0]->d_name+"/Init.txt";	//it's just the complete path to the init file of the selected directory
	ifstream fileselect(temp.c_str());	//and we open it to display some useful info; please refer to init.txt or Bluebird 
//first line
fileselect>>stemp;	//we take the name
lineone=stemp;
fileselect>>setw(6)>>stemp;		//the number of particles
lineone+=" : "+stemp+"		";
fileselect>>setw(6)>>stemp;	// dt
lineone+="dt : "+stemp+"ps";
//cout<<endl<<lineone<<endl;	
//secondline
fileselect>>setw(6)>>stemp;	//iterations
linetwo="It : "+stemp+"		";
fileselect>>setw(6)>>stemp;	//temperature
linetwo+="T : "+stemp+"K";
//cout<<endl<<linetwo<<endl;	
fileselect.close();
    bool go = true;	//boolean used to quit the menu and to launch the simu
    SDL_Event event;	//same as previously 
	int begin=0;	//the first directory display
	int end=0;	//and the last one
	if(count>=6){	//we display 6 directories max
		end=6;}
	else
		end=count;	//there is less than 6 directories in Data

	TTF_Font*	fontsel;	//We're going to use three different fonts, for the selected dir, for the other and for the informations
	TTF_Font* fontunsel;
	TTF_Font* fontinfo;
	temp=path+"GeosansLight.ttf";	//the font is in the CatBird folder
	const char * cpath=temp.c_str();
	fontsel = TTF_OpenFont(cpath, 50);	//we open the same font but with different sizes
	fontunsel = TTF_OpenFont(cpath, 30);
	fontinfo=TTF_OpenFont(cpath,20);
	int sel=0;	//we select the first folder
	SDL_Surface * sFontmen=NULL;	//the surface to render text
	SDL_Color color;	//color of the text
    while (go)
    {
	glMatrixMode(GL_MODELVIEW);	//ja, always matrix
	glPushMatrix();
	glLoadIdentity();
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	glDepthMask(GL_FALSE);  // disable writes to Z-Buffer, we are in two D
	glDisable(GL_DEPTH_TEST);  // disable depth-testing, 2D
	glTranslated(-320,-240,0);	//i prefer when 0 is not at the middle of the window!!!
//background
		glBegin(GL_QUADS);	//our background
		glColor3ub(200,200,255);	//it's a wonderful white-blue
		glVertex2f(0,0);
		glVertex2f( 0,480);
		glVertex2f(640,480 );
		glVertex2f(640,0);
		glEnd();
//text
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, width, 0, height); //2D
    glEnable(GL_TEXTURE_2D);	//we use texture to render text
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	glDepthMask(GL_FALSE);  // well, I use it more than I need...
	glDisable(GL_DEPTH_TEST); //but I'm not going to correct this

	glRotatef(180, 1, 0, 0.0f); // Rotate our object around the x axis  
	glEnable(GL_BLEND);	//well it's the same thing than the render text function of Box, it's really painful
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	GLuint texture;
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	j=begin;

/////////////////////////Render dir name
		int tempy=400;	//some coordinates, we are going to change tempy to display all folders to different height
		int yinfo=0;
		int xinfo=0;
		do{
		if(fontsel && fontunsel){
		if(j==sel)	//big font for the selected one
		{
			tempy-=10;
			color={50,50,255,0};	//a blue (RGBA)
			sFontmen=TTF_RenderText_Blended(fontsel, pDirenta[j]->d_name, color);	//we render the name of the directory, with the big font, in blue
			tempy-=40;	//and we change the position for the next one
			yinfo=sFontmen->h;	//we're going to put the info after this folder name
			xinfo=sFontmen->w;	//
		}
		else	//normal font for not selected one,
		{
			color={0,0,0,0};	//black
			sFontmen=TTF_RenderText_Blended(fontunsel, pDirenta[j]->d_name, color);	//same thing than for the selected one, but for unselected ones..;
			tempy-=30;
		}
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, sFontmen->w, sFontmen->h, 0, GL_BGRA, 	GL_UNSIGNED_BYTE, sFontmen->pixels);
	glBegin(GL_QUADS);	//now we put our text texture on simple quad
	{
		glTexCoord2f(0,0); glVertex2f(100, tempy);
		glTexCoord2f(1,0); glVertex2f(100 + sFontmen->w, tempy);
		glTexCoord2f(1,1); glVertex2f(100 + sFontmen->w, tempy + sFontmen->h);
		glTexCoord2f(0,1); glVertex2f(100, tempy + sFontmen->h);
	}
	glEnd();
	SDL_FreeSurface(sFontmen);	//same as previously but for displaying the info text, just need to be careful to where we put the text
	if(j==sel)
	{
	color={0,0,0};
	sFontmen=TTF_RenderText_Blended(fontinfo, linetwo.c_str(), color);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, sFontmen->w, sFontmen->h, 0, GL_BGRA, 	GL_UNSIGNED_BYTE, sFontmen->pixels);
	glBegin(GL_QUADS);
	{
		glTexCoord2f(0,0); glVertex2f(150+xinfo, tempy+yinfo/2.);
		glTexCoord2f(1,0); glVertex2f(150+xinfo + sFontmen->w,tempy+ yinfo/2.);
		glTexCoord2f(1,1); glVertex2f(150+xinfo + sFontmen->w,tempy+ yinfo/2. + sFontmen->h);
		glTexCoord2f(0,1); glVertex2f(150+xinfo, tempy+ yinfo/2. + sFontmen->h);
	}
	glEnd();
	//TTF_CloseFont(font); put this in the destructor
	SDL_FreeSurface(sFontmen);
	yinfo-=sFontmen->h+10;
	sFontmen=TTF_RenderText_Blended(fontinfo, lineone.c_str(), color);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, sFontmen->w, sFontmen->h, 0, GL_BGRA, 	GL_UNSIGNED_BYTE, sFontmen->pixels);
	glBegin(GL_QUADS);
	{
		glTexCoord2f(0,0); glVertex2f(150+xinfo, tempy+yinfo/2.);
		glTexCoord2f(1,0); glVertex2f(150+xinfo + sFontmen->w,tempy+ yinfo/2.);
		glTexCoord2f(1,1); glVertex2f(150+xinfo + sFontmen->w,tempy+ yinfo/2. + sFontmen->h);
		glTexCoord2f(0,1); glVertex2f(150+xinfo, tempy+ yinfo/2. + sFontmen->h);
	}
	glEnd();
	//TTF_CloseFont(font); put this in the destructor
	SDL_FreeSurface(sFontmen);
	}
	}


		j++;
		tempy-=20;
		}while(j<end and j<count);
	//render Titler
			color={0,0,0,0};
			sFontmen=TTF_RenderText_Blended(fontunsel, "Select a Directory", color);	//the title
//same as usual
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, sFontmen->w, sFontmen->h, 0, GL_BGRA, 	GL_UNSIGNED_BYTE, sFontmen->pixels);
	glBegin(GL_QUADS);
	{
		glTexCoord2f(0,0); glVertex2f(320-0.5*sFontmen->w, 5);
		glTexCoord2f(1,0); glVertex2f(320+0.5*sFontmen->w , 5);
		glTexCoord2f(1,1); glVertex2f(320+0.5*sFontmen->w, 5+ sFontmen->h);
		glTexCoord2f(0,1); glVertex2f(320-0.5*sFontmen->w, 5 + sFontmen->h);
	}
	glEnd();
	SDL_FreeSurface(sFontmen);
//Render ..
	if(end!=count)	//it means that there is more than 6 folders in data, so we put  "..." in order to inform the user that he can continue going up  to display the other ones 
	{
			color={0,0,0,0};
			sFontmen=TTF_RenderText_Blended(fontsel, "...", color);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, sFontmen->w, sFontmen->h, 0, GL_BGRA, 	GL_UNSIGNED_BYTE, sFontmen->pixels);
	glBegin(GL_QUADS);
	{
		glTexCoord2f(0,0); glVertex2f(100, 45);
		glTexCoord2f(1,0); glVertex2f(100+sFontmen->w , 45);
		glTexCoord2f(1,1); glVertex2f(100+sFontmen->w, 45+ sFontmen->h);
		glTexCoord2f(0,1); glVertex2f(100, 45 + sFontmen->h);
	}
	glEnd();
	//TTF_CloseFont(font); put this in the destructor
	SDL_FreeSurface(sFontmen);
	}
	if(begin!=0 )		//same but "..." at the bottom
	{
			color={0,0,0,0};
			sFontmen=TTF_RenderText_Blended(fontsel, "...", color);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, sFontmen->w, sFontmen->h, 0, GL_BGRA, 	GL_UNSIGNED_BYTE, sFontmen->pixels);
	glBegin(GL_QUADS);
	{
		glTexCoord2f(0,0); glVertex2f(100, 375);
		glTexCoord2f(1,0); glVertex2f(100+sFontmen->w , 375);
		glTexCoord2f(1,1); glVertex2f(100+sFontmen->w, 375+ sFontmen->h);
		glTexCoord2f(0,1); glVertex2f(100, 375 + sFontmen->h);
	}
	glEnd();
	//TTF_CloseFont(font); put this in the destructor
	SDL_FreeSurface(sFontmen);
	}
	glDisable(GL_BLEND);
	glDepthMask(GL_TRUE);  // disable writes to Z-Buffer
	glEnable(GL_DEPTH_TEST);
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();
	glDeleteTextures(1, &texture);
//pfiou




	glPopMatrix();
SDL_GL_SwapBuffers();

        SDL_WaitEvent(&event);		//just like in main()
        switch(event.type)
        {
            case SDL_QUIT:	//exit the program

				TTF_CloseFont(fontsel);
				TTF_CloseFont(fontunsel);	
				TTF_CloseFont(fontinfo);
                quit();
	
				//cout<<"test"<<endl;
			break;
			
			case SDL_KEYDOWN:
                switch (event.key.keysym.sym)

                {

                   case SDLK_ESCAPE:	//exit the program

					TTF_CloseFont(fontsel);
					TTF_CloseFont(fontunsel);
					TTF_CloseFont(fontinfo);
                    quit();
                    break;
					case SDLK_RETURN:	//simu !!!
					//go=false;
					TTF_CloseFont(fontsel);
					TTF_CloseFont(fontunsel);
					TTF_CloseFont(fontinfo);
					return pDirenta[sel]->d_name;
					break;
					case SDLK_UP :	//up and down arrow change the selected folder so we need to load a new init.txt file and do some other stuff
					sel++;
					if(sel>=count){
						sel=count-1;}
					else if(sel==end)
					{
						begin++;
						end++;
					}
					temp=path+"../Data/"+pDirenta[sel]->d_name+"/Init.txt";
					fileselect.open(temp.c_str());
					fileselect>>stemp;
					lineone=stemp;
					fileselect>>setw(6)>>stemp;
					lineone+=" : "+stemp+"		";
					fileselect>>setw(6)>>stemp;
					lineone+="dt : "+stemp+"ps";
	//				cout<<endl<<lineone<<endl;	
					fileselect>>setw(6)>>stemp;
					linetwo="It : "+stemp+"		";
					fileselect>>setw(6)>>stemp;
					linetwo+="T : "+stemp+"K";
		//			cout<<endl<<linetwo<<endl;	
					fileselect.close();
					break;
					case SDLK_DOWN :
					sel--;
					if(sel<0)
						sel=0;
					if(sel<begin)
					{
						begin--;
						end--;
					}
					temp=path+"../Data/"+pDirenta[sel]->d_name+"/Init.txt";
					fileselect.open(temp.c_str());
					fileselect>>stemp;
					lineone=stemp;
					fileselect>>setw(6)>>stemp;
					lineone+=" : "+stemp+"		";
					fileselect>>setw(6)>>stemp;
					lineone+="dt : "+stemp+"ps";
		//			cout<<endl<<lineone<<endl;	
					fileselect>>setw(6)>>stemp;
					linetwo="It : "+stemp+"		";
					fileselect>>setw(6)>>stemp;
					linetwo+="T : "+stemp+"K";
				//	cout<<endl<<linetwo<<endl;	
					fileselect.close();
					break;
				}
			break;
        }


    }
	TTF_CloseFont(fontsel);
	TTF_CloseFont(fontunsel);
	TTF_CloseFont(fontinfo);

}


void quit()
{
	SDL_FreeSurface(screen);
	TTF_Quit();
    SDL_Quit();
	exit(0);

}
